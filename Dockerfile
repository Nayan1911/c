FROM maven:latest AS builder

WORKDIR /app
COPY . .

RUN mvn clean install
COPY target/abc.jar .
CMD ["java", "-jar", "abc.jar"]

FROM openjdk:8-jdk-alpine
WORKDIR /app

COPY --from=builder /app/target/abc.jar .

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "abc.jar"]




